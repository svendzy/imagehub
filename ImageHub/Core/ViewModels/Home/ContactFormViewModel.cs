﻿using System.ComponentModel.DataAnnotations;

namespace ImageHub.Core.ViewModels.Home
{
    public class ContactFormViewModel
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Name is a required field")]
        [StringLength(50, ErrorMessage="Name must not exceed 50 characters")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Email is a required field")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Message is a required field")]
        [StringLength(1000, ErrorMessage="Message must not exceed 1000 characters")]
        public string Message { get; set; }
    }
}