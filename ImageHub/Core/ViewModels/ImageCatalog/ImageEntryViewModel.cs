﻿using System;

namespace ImageHub.Core.ViewModels.ImageCatalog
{
    public class ImageEntryViewModel
    {
        public int Id { get; set; }

        public string ThumbnailFile { get; set; }

        public string ImageFile { get; set; }

        public string Description { get; set; }

        public DateTime Modified { get; set; }

        public DateTime Uploaded { get; set; }

        public string UploadedBy { get; set; }

        public string Uploader { get; internal set; }
    }
}