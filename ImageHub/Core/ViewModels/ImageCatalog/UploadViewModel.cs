﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web;
using ImageHub.Core.Models;

namespace ImageHub.Core.ViewModels.ImageCatalog
{
    public class UploadViewModel
    {
        [DisplayName("Select Image:")]
        [Required(ErrorMessage = "ImageFile is a required field")]
        [ValidUploadFile(".jpeg;.jpg;.gif;.png", 2097152, ErrorMessage ="Invalid file extension or file size")]
        public HttpPostedFileBase ImageFile { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Description is a required field")]
        public string Description { get; set; }

        [DisplayName("Category")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Category is a required field")]
        public int CategoryId { get; set; }

        public IEnumerable<ImageCategory> CategoryItems { get; set; }
    }
}