﻿using System;

namespace ImageHub.Core.ViewModels.ImageCatalog
{
    public class UpdateEntryViewModel
    {
        public int Id { get; set; }
        public string ImageCategory { get; set; }
        public DateTime Uploaded { get; set; }
        public string UploadedBy { get; set; }
        public string Description { get; set; }
        public string ThumbnailFile { get; set; }
        public string ImageFile { get; set; }
    }
}