﻿using System.ComponentModel.DataAnnotations;
using System.Web;

namespace ImageHub.Core.ViewModels.ImageCatalog
{
    public class ValidUploadFile : ValidationAttribute
    {
        private string fileExts;
        private int maxFileSize;

        public ValidUploadFile(string fileExts, int maxFileSize)
        {
            this.fileExts = fileExts;
            this.maxFileSize = maxFileSize;
        }

        public override bool IsValid(object value)
        {
            var validFile = false;

            var fileBase = value as HttpPostedFileBase;
            if (fileBase == null)
                return false;
            if (fileBase.FileName == null)
                return false;
            if (fileBase.FileName == string.Empty)
                return false;
            if (fileExts == string.Empty)
                return false;
            if (fileBase.ContentLength > maxFileSize)
                return false;

            var exts =  fileExts.Split(new char[] {';'});
            foreach (var ext in exts)
            {
                if (fileBase.FileName.Contains(ext))
                {
                    validFile = true;
                    break;
                }
            }
            return validFile;
        }
    }
}