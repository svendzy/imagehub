﻿using ImageHub.Core;
using ImageHub.Core.Repositories;
using ImageHub.Persistence.Repositories;

namespace ImageHub.Persistence
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext _context;
        private readonly IFileRepository _fileRepository;
        private readonly IImageCatalogRepository _imageCatalogRepository;

        public UnitOfWork()
        {
            _context = new ApplicationDbContext();
            _fileRepository = new FileRepository();
            _imageCatalogRepository = new ImageCatalogRepository(_context);
        }

        public IFileRepository FileRepository
        {
            get { return _fileRepository; }
        }

        public IImageCatalogRepository ImageCatalog
        {
            get { return _imageCatalogRepository; }
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }
    }
}