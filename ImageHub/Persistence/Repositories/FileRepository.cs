﻿using ImageHub.Core.Repositories;
using ImageHub.Core.Utilities;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Web;

namespace ImageHub.Persistence.Repositories
{
    public class FileRepository : IFileRepository
    {
        public string GenerateFilenameBase(DateTime dateTime)
        {
            return $"{dateTime.Day}_{dateTime.Month}_{dateTime.Year}_{dateTime.Ticks}";
        }

        public void SaveImage(string mapPath, string filenameBase, HttpPostedFileBase imageData)
        {
            string imageFile = Path.Combine(mapPath, filenameBase + Path.GetExtension(imageData.FileName));
            using (var ms = new MemoryStream())
            {
                imageData.InputStream.Position = 0;
                imageData.InputStream.CopyTo(ms);
                File.WriteAllBytes(imageFile, ms.ToArray());
            }
        }

        public void SaveThumbnailImage(string mapPath, string filenameBase, HttpPostedFileBase imageData)
        {
            var thumbnailFile = Path.Combine(mapPath, filenameBase + "_thumb.jpg");
            using (var ms = new MemoryStream())
            {
                imageData.InputStream.Position = 0;
                imageData.InputStream.CopyTo(ms);
                Bitmap thumbnailImage = ThumbnailGenerator.Generate(Image.FromStream(ms), 80, 80);
                thumbnailImage.Save(thumbnailFile, ImageFormat.Jpeg);
            }
        }
    }
}