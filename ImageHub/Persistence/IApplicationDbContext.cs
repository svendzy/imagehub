using ImageHub.Core.Models;
using System.Data.Entity;

namespace ImageHub.Persistence
{
    public interface IApplicationDbContext
    {
        DbSet<ImageEntry> ImageEntries { get; set; }
        DbSet<ImageCategory> ImageCategories { get; set; }
        IDbSet<ApplicationUser> Users { get; set; }
    }
}