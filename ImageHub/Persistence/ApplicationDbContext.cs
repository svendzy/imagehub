using System.Data.Entity;
using ImageHub.Core.Models;
using Microsoft.AspNet.Identity.EntityFramework;

namespace ImageHub.Persistence
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>, IApplicationDbContext
    {
        public DbSet<ImageEntry> ImageEntries { get; set; }
        public DbSet<ImageCategory> ImageCategories { get; set; }

        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}