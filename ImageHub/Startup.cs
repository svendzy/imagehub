﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ImageHub.Startup))]
namespace ImageHub
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
