﻿using ImageHub.Core;
using ImageHub.Core.ViewModels.Home;
using System.Net;
using System.Net.Mail;
using System.Web.Mvc;

namespace ImageHub.Controllers
{
    public class HomeController : ControllerBase
    {
        public HomeController()
            : base(UnitOfWorkFactory.Create())
        {
        }

        [HttpGet]
        public ActionResult Index()
        {
            return RedirectToAction("Index", "ImageCatalog");
        }

        [HttpGet]
        public ActionResult About()
        {
           return View();
        }

        [HttpGet]
        public ActionResult Contact()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Contact(ContactFormViewModel contactForm)
        {
            if (!ModelState.IsValid)
            {
                return View("Contact", contactForm);
            }

            var smtpClient = new SmtpClient();
            smtpClient.Host = "smtp.gmail.com";
            smtpClient.Credentials = new NetworkCredential("user@gmail.com", "password");
            smtpClient.EnableSsl = true;

            var mailMessage = new MailMessage();
            mailMessage.From = new MailAddress("demo@svendzy-design.com");
            mailMessage.To.Add("svendzy.design@gmail.com");
            mailMessage.Subject = "Hello There";
            mailMessage.Body = "Someone sent you a message";

            smtpClient.Send(mailMessage);

            return RedirectToAction("Index", "Home");
        }
    }
}