﻿using ImageHub.Core;
using System.Web.Mvc;

namespace ImageHub.Controllers
{
    public abstract class ControllerBase : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public ControllerBase(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IUnitOfWork UnitOfWork
        {
            get { return _unitOfWork; }
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var imageCategories = _unitOfWork.ImageCatalog.GetCategories();
            ViewBag.ImageCategories = imageCategories;
            base.OnActionExecuting(filterContext);
        }
    }
}