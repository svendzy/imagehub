﻿using ImageHub.Core;
using ImageHub.Core.Models;
using ImageHub.Core.ViewModels.ImageCatalog;

using Microsoft.AspNet.Identity;
using System;
using System.IO;
using System.Linq;
using System.Web.Mvc;

namespace ImageHub.Controllers
{
    public class ImageCatalogController : ControllerBase
    {
        private const string ImagePath = "/Content/Uploads/";

        public ImageCatalogController() : base(UnitOfWorkFactory.Create())
        {
        }

        [HttpGet]
        public ActionResult Index()
        {
            // Prefetch list of image categories in case of large amount of images in database
            var imageCategories = UnitOfWork.ImageCatalog.GetCategories();

            // Prefetch list of users in case of large amount of images in database
            var userEntries = UnitOfWork.ImageCatalog.GetUserEntries()
                .Select(u => new { Id = u.Id, Name = u.UserName });

            // Get a list of images and project into UpdateEntryViewModel
            var updateEntries = UnitOfWork.ImageCatalog.GetLatestImages()
                .Select(m => new UpdateEntryViewModel
                {
                    Id = m.Id,
                    ImageCategory = imageCategories.Single(c => c.Id == m.CategoryId).Name,
                    ThumbnailFile = ImagePath + m.FilenameBase + "_thumb.jpg",
                    ImageFile = ImagePath + m.FilenameBase + m.FileExt,
                    Description = m.Description,
                    UploadedBy = userEntries.Single(c => c.Id == m.Uploader).Name,
                    Uploaded = m.Uploaded
                });

            // Construct UpdateEntriesViewModel
            var viewModel = new UpdateEntriesViewModel
            {
                UpdateEntries = updateEntries
            };

            return View(viewModel);
        }

        [HttpGet]
        public ActionResult Categories(int? id)
        {
            if (id == null)
                return new HttpNotFoundResult();

            // Prefetch list of image categories in case of large amount of images in database
            var imageCategory = UnitOfWork.ImageCatalog.GetCategoryById(id);
            if (imageCategory == null)
                return new HttpNotFoundResult();

            // Prefetch list of users in case of large amount of images in database
            var userEntries = UnitOfWork.ImageCatalog.GetUserEntries()
                .Select(u => new { Id = u.Id, Name = u.UserName });

            // Get a list of images by category id and project into ImageEntryViewModel
            var imageEntries = UnitOfWork.ImageCatalog.GetImagesByCategory(id)
                .Select(m => new ImageEntryViewModel
                {
                    Id = m.Id,
                    ThumbnailFile = ImagePath + m.FilenameBase + "_thumb.jpg",
                    ImageFile = ImagePath + m.FilenameBase + m.FileExt,
                    Description = m.Description,
                    Modified = m.Modified,
                    Uploaded = m.Uploaded,
                    UploadedBy = userEntries.Single(u => u.Id == m.Uploader).Name,
                    Uploader = userEntries.Single(u => u.Id == m.Uploader).Id
                });

            // Construct CategoryViewModel
            var viewModel = new CategoryViewModel
            {
                CategoryName = imageCategory.Name,
                ImageEntries = imageEntries
            };
            return View("Category", viewModel);
        }

        [HttpGet]
        [Authorize]
        public ActionResult Upload()
        {
            // Construct UploadViewModel
            var viewModel = new UploadViewModel
            {
                CategoryItems = UnitOfWork.ImageCatalog.GetCategories()
            };
            return View(viewModel);
        }

        [HttpPost]
        [Authorize]
        public ActionResult Upload(UploadViewModel viewModel)
        {
            // Server-side validation check
            if (!ModelState.IsValid)
            {
                viewModel.CategoryItems = UnitOfWork.ImageCatalog.GetCategories();
                return View("Upload", viewModel);
            }
            
            var dateTime = DateTime.Now;
            var filenameBase = UnitOfWork.FileRepository.GenerateFilenameBase(dateTime);
            UnitOfWork.FileRepository.SaveImage(
                Server.MapPath($"~{ImagePath}"), filenameBase, viewModel.ImageFile);
            UnitOfWork.FileRepository.SaveThumbnailImage(
                Server.MapPath($"~{ImagePath}"), filenameBase, viewModel.ImageFile);

            var imgEntry = new ImageEntry
            {
                FilenameBase = filenameBase,
                FileExt = Path.GetExtension(viewModel.ImageFile.FileName),
                Description = viewModel.Description,
                CategoryId = viewModel.CategoryId,
                Modified = dateTime,
                Uploaded = dateTime,
                Uploader = User.Identity.GetUserId()
            };

            UnitOfWork.ImageCatalog.AddImage(imgEntry);
            UnitOfWork.SaveChanges();

            return RedirectToAction("Index", "ImageCatalog");
        }
    }
}